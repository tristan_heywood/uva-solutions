#include <cstdio>
#include <algorithm>
#include <iostream>
#include <utility>
#include <cstring>
#include <vector>
#include <bitset>
#include <cmath>
#include <cstring>

#define getD(x) scanf("%d", &x)
using namespace std;
int bcc[52][52];
int cutLocs[1002];

int best_cut_cost(int left, int right) {
    if (left + 1 == right) return 0;
    if (bcc[left][right] != -1) return bcc[left][right];
    int len = cutLocs[right] - cutLocs[left];
    int best = 10000000;
    for(int cut = left + 1; cut < right; cut++) {
        int cost = best_cut_cost(left, cut);
        cost += best_cut_cost(cut, right);
        best = min(best, cost);
    }
    bcc[left][right] = best + len;
    return best + len;
}
int main() {
    int l, n;
    while(getD(l), l) {
        getD(n);
        for(int i = 0; i < n + 2; i++) {
            for(int j = 0; j < n + 2; j++) {
                bcc[i][j] = -1;
            }
        }
        for(int i = 1; i < n + 1; i++) {
            getD(cutLocs[i]);
        }
        cutLocs[0] = 0;
        cutLocs[n+1] = l;
        int best = best_cut_cost(0, n + 1);
        printf("The minimum cutting is %d.\n", best);
    }
}
