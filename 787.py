while True :
	try :
		line = input()
		if line == '' : break
	except :
		break 
	nums = list(map(int, line.split()[:-1]))
	
	prevMPos = nums[0]
	prevMNeg = nums[0]

	maxi = prevMPos	

	for prevInd, num in enumerate(nums[1:]) :
		maxPos = max(num * prevMPos, num * prevMNeg, num)
		maxNeg = min(num * prevMPos, num * prevMNeg, num)
		prevMPos = maxPos
		prevMNeg = maxNeg
		maxi = max(maxi, maxPos)

	print(maxi)
		

	