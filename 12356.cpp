#include <iostream>
#include <cstdio>

using namespace std;

int leftS[100000];
int rightS[100000];
int main() {
    int s, b;
    while (cin >> s >> b, s != 0 && b != 0) {
        //cin >> b;
        //cout << s <<endl;
        for (int i = 0; i < s; i++) {
            leftS[i] = i-1;
            rightS[i] = i+1;
        }
        for(int i = 0; i < b; i++) {
            int l, r;
            cin >> l >> r;
            l--; r--;
            if(leftS[l] > -1) {
                rightS[leftS[l]] = rightS[r];
                printf("%d ", leftS[l] + 1);
            }
            else printf("* ");
            if(rightS[r] < s) {
                leftS[rightS[r]] = leftS[l];
                printf("%d\n", rightS[r] + 1);
            }
            else printf("*\n");
        }
        cout << "-" << endl;
    }
}
