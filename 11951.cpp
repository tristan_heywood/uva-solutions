#include <cstdio>

#define getD(x) scanf("%d", &x);

int main() {
    int tc; getD(tc);
    int caseNum = 0;
    int district[100][100];
    while(tc--) {
        caseNum++;
        int n, m, k; getD(n); getD(m); getD(k);
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < m; j++) {
                int val; getD(val);
                if (i > 0) val += district[i-1][j];
                if (j > 0) val += district[i][j-1];
                if (i > 0 && j > 0) val -= district[i-1][j-1];
                district[i][j] = val;
            }
        }
        int maxArea = -1;
        int bestCost = 10000000;
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < m; j++) {
                for(int x = i; x < n; x++) {
                    for(int y = j; y < m; y++) {
                        int area = (x-i+1) * (y-j+1);
                        if (area < maxArea) continue;
                        int cost = district[x][y];
                        if (i > 0) cost -= district[i-1][y];
                        if (j > 0) cost -= district[x][j-1];
                        if (i > 0 && j > 0) cost += district[i-1][j-1];
                        if (cost > k || (area == maxArea && cost >= bestCost)) continue;
                        maxArea = area;
                        bestCost = cost;
                    }
                }
            }
        }
        if (maxArea == -1) {
            maxArea = 0;
            bestCost = 0;
        }
        printf("Case #%d: %d %d\n", caseNum, maxArea, bestCost);
    }
}
