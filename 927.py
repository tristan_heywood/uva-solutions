def get_n(poly, n) :
    val = 0
    for i, c in enumerate(poly) :
        val += n**i * c
    return val

for tc in range(int(input())) :
    poly = [int(x) for x in input().split()][1:]
    d = int(input())
    k = int(input())
    for p in range(10**10) :
        if d * (p**2 + p) / 2 >= k :
            print(get_n(poly, p))
            break
