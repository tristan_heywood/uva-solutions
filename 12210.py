import collections
for caseNum, (B, S) in enumerate(iter(lambda : [int(x) for x in input().split()], [0, 0]), 1) :
    bachs = [int(input()) for i in range(B)]
    spins = [int(input()) for i in range(S)]
    bachs.sort(reverse = True)
    if B > S :
        print('Case {}: {} {}'.format(caseNum, B - S, bachs[-1]))
    else :
        print('Case {}: 0'.format(caseNum))
    #spinsAges = collections.Counter(spins)
    # for ind, bach in enumerate(bachs) :
    #     ageDist = 0
    #     hi = 0
    #     lo = 0
    #     while hi != 60 or lo != 2 :
    #         hi = min(60, bach + ageDist)
    #         lo = max(2, bach - ageDist)
    #         if spinsAges[hi] > 0 :
    #             spinsAges[hi] -= 1
    #             break
    #         if spinsAges[lo] > 0 :
    #             spinsAges[lo] -= 1
    #             break
    #     else :
    #         break
