import bisect
import sys
for line in sys.stdin :
	n = int(line.strip())
	prices = list(map(int, input().split()))
	money = int(input())
	prices.sort()

	bestPair = None
	bestDist = 1000000

	for lowInd, lowPrice in enumerate(prices) :
		upperInd = bisect.bisect_left(prices, money - lowPrice) 
		if upperInd < len(prices) and prices[upperInd] + lowPrice == money and abs(prices[upperInd] - lowPrice) < bestDist :
			bestPair = (lowPrice, prices[upperInd])
			bestDist = prices[upperInd] - lowPrice
	print('Peter should buy books whose prices are {} and {}.\n'.format(*bestPair))
	input()
