#include <cstdio>
#include <bitset>
using namespace std; 

bool boardSpotBad[14][14];
int n; 
int ans; 

bitset<30> rowsFull, leftDiagsFull, rightDiagsFull; 
void find_perm(int colNum) {
	if (colNum == n) {
		ans++; 
		return; 
	}
	for(int rowNum = 0; rowNum < n; rowNum++) {
		if(!boardSpotBad[rowNum][colNum] && !rowsFull[rowNum] 
			&& !leftDiagsFull[rowNum - colNum + n - 1]
			&& !rightDiagsFull[rowNum + colNum]
			) 

		{
			rowsFull[rowNum] = true; 
			leftDiagsFull[rowNum - colNum + n - 1] = true; 
			rightDiagsFull[rowNum + colNum] = true; 

			find_perm(colNum + 1); 
			rowsFull[rowNum] = false; 
			leftDiagsFull[rowNum - colNum + n - 1] = false; 
			rightDiagsFull[rowNum + colNum] = false; 
		}
	}
}

int main() {
	rowsFull.reset(), leftDiagsFull.reset(), rightDiagsFull.reset();
	n = 0;
	ans = 0; 
	int testCase = 0; 
	while(scanf("%d", &n), n) {
		testCase++; 
		ans = 0; 

		for(int i = 0; i < n; i++) {
			for(int j = 0; j < n; j++) {
				char ch; 
				scanf("%c", &ch); 
				boardSpotBad[i][j] = ch == '*' ? 1 : 0; 
				if (ch == '\n') j--;  
			}
		}

		find_perm(0); 
		printf("Case %d: %d\n", testCase, ans); 
		ans++;
	}
	
}