for i in range(int(input())) :
	n = input()
	coins = list(map(int, input().split()))

	withdraw = 1
	numCoins = 2
	for nextInd, c in enumerate(coins[1:-1],2) :
		if withdraw + c < coins[nextInd] :
			withdraw += c
			numCoins += 1
	print(numCoins)
