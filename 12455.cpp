#include <cstdio>

int bars[30]; 

int main() {
	int t, n, p, sum; 
	scanf("%d", &t); 
	while(t--) {
		scanf("%d\n%d", &n, &p);
		for(int i = 0; i < p; i++) {
			scanf("%d", &bars[i]); 
		}
		for(long long i = 0; i < (1<<p); i++) {
			sum = 0; 
			for(int j = 0; j < p; j++) {
				if (i & (1 << j)) {
					sum += bars[j]; 
				}
			}
			if (sum == n) break; 
		}
		if(sum == n) {
			printf("YES\n"); 
		}
		else {
			printf("NO\n");
		}
	}
}