#include <cstdio>
#include <algorithm>
#include <iostream>

#define getD(x) scanf("%d", &x)
using namespace std;

int n, t;
int nums[12];
bool used[12];
bool solnFound;
void solve(int total, int ind) {

    if(total == 0) {
        solnFound = true;
        bool first = true;
        for(int i = 0; i < n; i++) {
            if(used[i]) {
                if(!first) {
                    printf("+");
                }
                printf("%d", nums[i]);
                first = false;
            }

        }
        printf("\n");
        return;
    }
    if(total < 0 || ind == n) return;
    int picked[12];
    int nextPicked = 0;
    for(int i = ind; i < n; i++) {
        //don't pick the two duplicates of the same number twice at the same depth
        for(int j = 0; j < nextPicked; j++) {
            if(picked[j] == nums[i]) goto endLoop;
        }
        if(!used[i]) {
            used[i] = true;
            picked[nextPicked++] = nums[i];
            solve(total - nums[i], i + 1);
            used[i] = false;
        }
        endLoop:;
    }
}

int main() {
    while(scanf("%d %d", &t, &n), n != 0) {
        solnFound = false;
        for(int i = 0; i < n; i++) {
            getD(nums[i]);
        }
        printf("Sums of %d:\n", t);
        solve(t, 0);
        if(!solnFound) printf("NONE\n");
    }

}
