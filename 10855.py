
for N, n in iter(lambda: [int(x) for x in input().split()], [0, 0]) :
    bigSquare = [input() for x in range(N)]
    smallSquare = [input() for x in range(n)]
    squaresDict = {}
    for topLeftRow in range(N - n + 1) :
        for topLeftCol in range(N - n + 1) :
            square = ''.join(bigSquare[topLeftRow + i][topLeftCol:topLeftCol+n] for i in range(n))
            if square not in squaresDict :
                squaresDict[square] = 1
            else :
                squaresDict[square] += 1
    appears = []
    for i in range(4) :
        smallSquare = list(zip(*smallSquare[::-1]))
        square = ''.join(''.join(row) for row in smallSquare)
        if square in squaresDict :
            appears.append(squaresDict[square])
        else :
            appears.append(0)
    appears = [appears[-1]] + appears[:-1]
    print(*appears)
