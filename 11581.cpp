#include <cstdio>
#include <cstring>
#define getD(x) scanf("%d", &x)
#define putD(x) printf("%d\n", x)
using namespace std;


int main() {
    int tc; getD(tc);
    while (tc--) {
        int grid[3][3];
        int nextGrid[3][3];
        int seen[1024];
        for(int i = 0; i < 1024; i++) {
            seen[i] = -1;
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                scanf(" %c", &grid[i][j]);
                grid[i][j] -= '0';
            }
        }
        int index = 0;
        int seenVal = 0;
        for(int i = 0; i < 9; i++) seenVal += ((int*)grid)[i] << i;
        while (seen[seenVal] == -1) {
            seen[seenVal] = index;
            seenVal = 0;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    int res = 0;
                    if (i > 0) res += grid[i-1][j];
                    if (i < 2) res += grid[i+1][j];
                    if (j > 0) res += grid[i][j-1];
                    if (j < 2) res += grid[i][j+1];
                    nextGrid[i][j] = res % 2;
                    seenVal += nextGrid[i][j] << (3 * i + j);
                }
            }
            memcpy(grid, nextGrid, 9 * sizeof(int));
            index++;
        }
        if (index == 0) putD(-1);
        else putD(seen[seenVal] - 1);
    }
}
