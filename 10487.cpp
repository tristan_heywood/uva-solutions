#include <cstdio>
#include <stdlib.h>
#include <iostream>
#define getD(x) scanf("%d", &x)

using namespace std;

int main() {
    int n;
    int nums[1000];
    int testCase = 0;
    while(cin >> n) {
        if(n == 0) break;
        testCase++;
        for(int i = 0; i < n; i++) {
            getD(nums[i]);
        }
        int m; getD(m);

        printf("Case %d:\n", testCase);
        for(int a = 1; a <= m; a++) {
            int query; getD(query);

            int bestSum= 1 << 20;

            for(int i = 0; i < n; i++) {
                for(int j = i + 1; j < n; j++) {
                    if(abs(nums[i] + nums[j] - query) < abs(bestSum - query)) {
                        bestSum = nums[i] + nums[j];
                    }
                }
            }
            printf("Closest sum to %d is %d.\n", query, bestSum);


        }
    }
}
