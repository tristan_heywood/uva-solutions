#include <cstdio>
#include <vector>

using namespace std;

int main() {
    int n;
    while (scanf("%d", &n), n != 0) {
        int numOnes = 0;
        int a = 0;
        int b = 0;
        for (int index = 0; index < 31; index++) {
            if (n & (1 << index)) {
                numOnes++;
                if (numOnes % 2 == 0) {
                    b |= (1 << index);
                }
                else a |= (1 << index);
            }
        }
        printf("%d %d\n", a, b); 
    }
}
