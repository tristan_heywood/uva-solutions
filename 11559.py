while True :
	try :
		n, b, h, w = map(int, input().split())
	except :
		break
	minCost = b + 1
	for i in range(h) :
		p = int(input())
		bedsAtWeek = map(int, input().split())

		if p * n < minCost :
			for numBeds in bedsAtWeek :
				if numBeds >= n :
					minCost = p * n
					break
	print(minCost if minCost <= b else 'stay home')