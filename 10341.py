import sys
from math import *
#for i in range(1) :
for line in sys.stdin.readlines() :
    p, q, r, s, t, u = map(float, line.split())
    #p, q, r, s, t, u = (12, -10, 8, 0, -1, 14,)
    lo = 0.0
    hi = 1.0
    func = lambda x : p * e**-x + q * sin(x) + r * cos(x) + s * tan(x) + t * x**2 + u
    while abs(hi - lo) > 10**-14 :
        mid = (hi + lo) / 2
        #print(hi, lo, mid, func(mid))
        if  func(mid) > 0 :
            lo = mid
        else :
            hi = mid

    if abs(func(hi) - 0) > 10**-9  :
        print('No solution')
    else :
        print('{0:.4f}'.format(hi))
