#include <cstdio>
#include <cstdlib>
#include <algorithm>

using namespace std;

int main() {
    int M;
    char square[100][100];
    while(scanf("%d\n", &M) == 1) {
        for(int i = 0; i < M; i++) {
            scanf("%s\n", square[i]);
        }
        int maxDist = 0;
        for(int i = 0; i < M; i++) {
            for(int j = 0; j < M; j++) {
                if(square[i][j] == '1') {
                    int bestDist = 2 << 10;
                    for(int x = 0; x < M; x++) {
                        for(int y = 0; y < M; y++) {
                            if(square[x][y] == '3') {
                                bestDist = min(abs(i - x) + abs(j - y), bestDist);
                            }
                        }
                    }
                    maxDist = max(bestDist, maxDist);
                }
            }
        }
        printf("%d\n", maxDist);
    }
}
