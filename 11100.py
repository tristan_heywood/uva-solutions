from itertools import groupby
import itertools
first = True
while True :


	k = int(input())
	if k == 0 : break

	if not first : print()
	first = False
	dims = []
	while len(dims) != k :
		dims += [int(x) for x in input().split()]

	dims.sort()
	maxOccur = max((len(list(a[1])) for a in groupby(dims)))
	bags = [[] for a in range(maxOccur)]

	pieces = iter(dims)
	for bag in itertools.cycle(bags) :
		try :
			bag.append(next(pieces))
		except :
			break

	print(maxOccur)
	for bag in bags :
		print(' '.join((str(a) for a in bag)))
