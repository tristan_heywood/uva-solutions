#include <iostream>
#include <string>
#include <vector>
#include <stack>

using namespace std;

vector<vector<char> > sequences;
string source;
string target;
void find_seqs(stack<char> letterStack, vector<char> history, int sourceInd, int targInd) {

    if (targInd == target.size()) {
        sequences.push_back(history);
    }
    if (!letterStack.empty() && letterStack.top() == target[targInd]) {
        //pop first
        stack<char> letterStackCopy = letterStack;
        letterStackCopy.pop();
        vector<char> historyCopy = history;
        historyCopy.push_back('o');
        find_seqs(letterStackCopy, historyCopy, sourceInd, targInd+1);

    }
    if (sourceInd < source.size()) {
        letterStack.push(source[sourceInd]);
        history.push_back('i');
        find_seqs(letterStack, history, sourceInd+1, targInd);
    }


}
int main() {

    while(getline(cin, source)) {
        getline(cin, target);
        sequences.clear();
        stack<char> letterStack;
        vector<char> history;
        find_seqs(letterStack, history, 0, 0);
        cout << '[' << endl;
        for (int i = sequences.size()-1; i >= 0; i--) {
            for (int j = 0; j < sequences[i].size(); j++) {
                if (j) cout << " "; 
                cout << sequences[i][j];

            }
            cout << endl;
        }
        cout << ']' << endl;


    }

}
