#include <cstdio>

#define getD(x) scanf("%d", &x)

using namespace std;

int main() {
    int tc;
    getD(tc);
    while(tc--) {
        int d;
        getD(d);
        int low[10000], high[10000];
        char name[10000][25];
        for(int i = 0; i < d; i++) {
            scanf("%s %d %d", name[i], &low[i], &high[i]);
        }
        int q;
        getD(q);
        while(q--) {
            int price;
            getD(price);
            int numValid = 0;
            int nameInd = 0;
            for(int i = 0; i < d; i++) {
                if(price >= low[i] && price <= high[i]) {
                    numValid++;
                    nameInd = i;
                }
                if(numValid > 1) {
                    break;
                }
            }
            if(numValid == 1) {
                printf("%s\n", name[nameInd]);
            }
            else {
                printf("UNDETERMINED\n"); 
            }
        }
        if(tc) {
            printf("\n");
        }
    }
}
