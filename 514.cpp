#include <iostream>
#include <stack>

using namespace std;

int main() {
    int n;
    bool first = true;
    while(cin >> n, n) {


        int target[1000];

        while(cin >> target[0], target[0]) {
            stack<int> station;
            for (int i = 1; i < n; i++) cin >> target[i];

            int nextInd = 0;

            for (int i = 1; i <= n || nextInd < n; ) {
                if (!station.empty() && station.top() == target[nextInd]) {
                    station.pop();
                }
                else if (i <= target[nextInd]) {
                    while (i <= target[nextInd]) station.push(i++);
                    station.pop();
                }
                else {
                    goto impossible;
                }
                nextInd++;
            }

            cout << "Yes" << endl;
            continue;
            impossible:
            cout << "No" << endl;
        }
        cout << endl;
    }
}
