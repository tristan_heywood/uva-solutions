
while True :
    l = input()
    if 0 == int(l.strip()) : break

    bends = input().split()
    curDir = '+x'

    for bend in bends :

        if bend == 'No' :
            continue
        if curDir == '+x' :
             curDir = bend
        elif curDir == '-x':
            curDir = ('-' if bend[0] is '+' else '+') + bend[1]
        elif bend[1] == curDir[1] :
            if bend[0] == curDir[0] :
                curDir = '-x'
            else :
                curDir = '+x'

    print(curDir)
