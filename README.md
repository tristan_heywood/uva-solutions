# UVA Solutions

A combination of Python3 and C++11 solutions to various problems from the UVA live archive. All solutions here have been accepted by the online judge. 