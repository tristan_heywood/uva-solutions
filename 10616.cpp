#include <iostream>
#include <cstdio>
#include <cstring>
using namespace std;

long long Memo[20][200][10]; //M[i][j][k] = number of ways of summing to i mod d, using k of the first j elements.
int S[200];
int N, D, Q, M;
long long numWays(int sum, int maxInd, int numElems) {
    if (numElems == 0 && sum == 0) return 1;
    if (maxInd == -1 || numElems == 0) return 0;

    if (Memo[sum][maxInd][numElems] != -1) return Memo[sum][maxInd][numElems];

    long long ways = numWays(sum%D, maxInd-1, numElems);
    ways += numWays(((sum+S[maxInd])%D + D)%D, maxInd-1, numElems-1);
    Memo[sum][maxInd][numElems] = ways;
    return ways;
}
int main() {
    int setNum = 1;
    while(cin >> N >> Q, N != 0 || Q != 0) {

        printf("SET %d:\n", setNum++);
        for (int i = 0; i < N; i++) {
            cin >> S[i];
        }
        for (int i = 0; i < Q; i++) {
            memset(Memo, -1, sizeof(Memo)); 
            cin >> D >> M;
            long long ways = numWays(0, N-1, M);
            printf("QUERY %d: %lld\n", i+1, ways);
        }
    }
}
