import itertools
import bisect
candidates = input()
serials = {chr(x) : [] for x in itertools.chain(range(ord('a'), ord('z') + 1), range(ord('A'), ord('Z') + 1))}

for ind, s in enumerate(candidates) : 
	serials[s].append(ind)

q = int(input())

for i in range(q) :
	query = input()
	nextSer = 0
	firstSer = -1
	for s in query :
		serList = serials[s]
		listInd = bisect.bisect_left(serList, nextSer)

		if listInd >= len(serList) :
			print('Not matched')
			break

		if firstSer == -1 :
			firstSer = serList[listInd]
		nextSer = serList[listInd] + 1
	else :
		print('Matched {} {}'.format(firstSer, nextSer-1))