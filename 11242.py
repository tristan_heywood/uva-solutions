while True :
    try :
        f, r = [int(x) for x in input().split()]
    except :
        break
    frontCluster = [int(x) for x in input().split()]
    rearCluster = [int(x) for x in input().split()]

    ratios = [rear / front for rear in rearCluster for front in frontCluster]
    ratios.sort()
    spreads = [d2 / d1 for d2, d1 in zip(ratios[1:], ratios)]
    print('{0:.2f}'.format(max(spreads)))
