import itertools

#all permutations for which the function returns true
#assumes that if the function returns false for any permutation p
#it will also return false for any perumutation with p as a prefix
# def perms_satisfying(elems, func, prefix = []) :
# 	if len(elems) == 0 :
# 		return prefix
		
# 	for e in elems :
# 		if func(prefix + e) :
# 			return perms_satisfying(elems - e)
			


first = True

for tc in range(int(input())) :
	input()
	qRow, qCol = [int(x) - 1 for x in input().split()]

	queens = list(range(8))
	del queens[qRow]

	validPerms = []
	for perm in itertools.permutations(queens) :
		perm = list(perm)
		perm.insert(qCol, qRow)

		for col, row in enumerate(perm) :
			for colIt, rowIt in enumerate(perm[col+1:]) :
				colIt += (col + 1)
				if col == colIt or row == rowIt \
					or abs(colIt - col) == abs(rowIt - row) :
					break
			else :
				continue
			break
		else :
			validPerms.append(perm)

	if not first :
		print()
	first = False
	print('SOLN       COLUMN')
	print(' #      1 2 3 4 5 6 7 8\n')
	for ind, vp in enumerate(validPerms, 1):
		print('{:>2}     '.format(ind), *[x+1 for x in vp])
