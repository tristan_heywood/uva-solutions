gInts = lambda: [int(x) for x in input().split()]

def get_optimal(garmInd, money) :
    if money < 0: return -1e9


    if opt[garmInd][money] != -1 : return opt[garmInd][money]

    if garmInd == 0 :
        bestGar = max((g for g in garments[0] + [-1e9] if g <= money))
        opt[garmInd][money] = bestGar
        return bestGar

    best = -1e9
    for garm in garments[garmInd] :
        best = max(get_optimal(garmInd-1, money - garm) + garm, best)
    opt[garmInd][money] = best
    return best



for tc in range(gInts()[0]) :
    m, c = gInts()
    garments = [sorted(gInts()[1:]) for i in range(c)]
    #opt[g][m] = max price <= m with optimal solution for first g garments with m money
    opt = [[-1 for i in range(m+5)] for j in range(c)]
    best = get_optimal(c-1, m)
    if best < 0 :
        print('no solution')
    else :
        print(best)
    # for garmInd in range(c) :
    #     print([(m, o) for m, o in enumerate(opt[garmInd]) if o != -1])
