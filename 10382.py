import sys
for n, l, w in (map(int, x.split()) for x in iter(lambda: sys.stdin.readline(), '')) :
    sprinks = [list(map(int, input().split())) for i in range(n)]
    intervals = []
    for sp in sprinks :
        halfWidth = (max(0, sp[1]**2 - (w/2)**2))**0.5
        intervals.append((min(max(0, sp[0] - halfWidth), l), max(min(sp[0] + halfWidth, l), 0)))
    intervals.sort()
    swLine = 0
    swInd = 0
    numIntervals = 0
    #print(intervals)
    while swLine != l:
        best = None
        bestRight = -1
        for i in range(swInd, n) :
            if intervals[i][0] > swLine : break
            if intervals[i][1] > bestRight :
                bestRight = intervals[i][1]
                best = (*intervals[i], i)
        if best is None :
            break
        swLine = best[1]
        swInd = best[2] + 1
        numIntervals += 1
    if swLine != l :
        print(-1)
    else :
        print(numIntervals)
