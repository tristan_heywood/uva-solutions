for i in range(int(input())) :
	n , field = int(input()), input()
	numNeeded, x = 0, 0
	while x < n :
		x, numNeeded = (x + 3 if field[x] == '.' else x + 1), (numNeeded + 1 if field[x] == '.' else numNeeded)
	print('Case {}: {}'.format(i + 1, numNeeded))
