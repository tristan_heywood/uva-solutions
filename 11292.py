import sys
for n, m in (map(int, x.split()) for x in iter(sys.stdin.readline, '0 0\n')) :
    heads = [int(input()) for i in range(n)]
    knights = [int(input()) for i in range(m)]
    heads.sort()
    knights.sort()
    knInd = 0
    gold = 0
    for head in heads :
        while knInd < m and knights[knInd] < head : knInd += 1
        if knInd == m :
            break
        gold += knights[knInd]
        knInd += 1
    else :
        print(gold)
        continue
    print('Loowater is doomed!')
