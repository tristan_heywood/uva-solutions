#include <cstdio>
#include <algorithm>
#include <iostream>
#include <utility>
#include <cstring>
#include <vector>
#include <bitset>
#include <cmath>

#define getD(x) scanf("%d", &x)
using namespace std;

int n;
int rungs[100000];

bool isPossible(long k) {
    long curHeight = 0;
    for(int i = 0; i < n; i++) {
        long diff = rungs[i] - curHeight;
        if(diff > k) {
            return false;
        } else if (diff == k) {
            k--;
        }
        curHeight = rungs[i];
    }
    return true;
}

int main() {
    int tc; getD(tc);
    for(int caseNum = 1; caseNum <= tc; caseNum++) {
        getD(n);
        for(int i = 0; i < n; i++) {
            getD(rungs[i]);
        }
        long lo = 1;
        long hi = 10000000;
        long mid = 0;
        while(abs(hi - lo) > 1) {
            mid = (hi + lo) / 2;
            if(isPossible(mid)) {
                hi = mid;
            } else {
                lo = mid;
            }
        }
        if(!isPossible(mid)) mid++;
        if(isPossible(mid-1)) mid--;
        printf("Case %d: %ld\n", caseNum, mid);

    }
}
