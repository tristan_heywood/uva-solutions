#include <cstdio>
#include <algorithm>
#include <iostream>
#include <utility>
#include <cstring>
#include <vector>

#define getD(x) scanf("%d", &x)
using namespace std;

vector<int> edgeVectors[100];

vector<int> curSolution;
vector<int> bestSolution;

bool isBlack[100];

int n, k;
int maxBlack;

bool hasBlackAdj(int vertex) {
    for(int i = 0; i < edgeVectors[vertex].size(); i++) {
        if(isBlack[edgeVectors[vertex][i]]) {
            return true;
        }
    }
    return false;
}

void find_soln(int vertex, int numBlack) {
    if(vertex == n) {
        if(numBlack > maxBlack) {
            bestSolution = curSolution;
            maxBlack = numBlack;
        }
        return;
    }
    if(!hasBlackAdj(vertex)) {
        curSolution.push_back(vertex);
        isBlack[vertex] = true;
        find_soln(vertex + 1, numBlack + 1);
        curSolution.pop_back();
        isBlack[vertex] = false;

    }
    find_soln(vertex + 1, numBlack);
}

int main() {

    int m; getD(m);
    for(int tc = 0; tc < m; tc++) {
        maxBlack = 0;
        for(int i = 0; i < n; i++) isBlack[i] = false;
        curSolution.clear();
        getD(n); getD(k);
        for(int i = 0; i < n; i++) {
            edgeVectors[i].clear();
        }
        for(int i = 0; i < k; i++) {
            int s, f; getD(s); getD(f);
            s--;
            f--;
            edgeVectors[s].push_back(f);
            edgeVectors[f].push_back(s);
        }
        find_soln(0, 0);

        curSolution.push_back(0);
        isBlack[0] = true;
        find_soln(1, 1);

        printf("%d\n", maxBlack);
        for(int i = 0; i < bestSolution.size() - 1; i++) printf("%d ", bestSolution[i]+1);
        printf("%d\n", bestSolution[bestSolution.size() -1]+1);
    }

}
