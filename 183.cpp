#include <cstdio>
#include <algorithm>
#include <iostream>
#include <utility>
#include <cstring>
#include <vector>
#include <bitset>
#include <cmath>

#define getD(x) scanf("%d", &x)
using namespace std;
char bitmap[200][200];
char output[40000];
char * outPtr;

void processB(int row, int col, int rowMax, int colMax) {

    if(row == rowMax || col == colMax) return;

    char type = bitmap[row][col];
    for(int i = row; i < rowMax; i++) {
        for(int j = col; j < colMax; j++) {
            if (bitmap[i][j] != type) goto afterLoop;
        }
    }
    *outPtr++ = type;
    return;

    afterLoop:;

    *outPtr++ = 'D';
    int rowMid = (row + rowMax + 1)/2;
    int colMid =  (col + colMax + 1)/2;
    processB(row, col, rowMid, colMid);
    processB(row, colMid, rowMid, colMax);
    processB(rowMid, col, rowMax, colMid);
    processB(rowMid, colMid, rowMax, colMax);

}
void processD(int row, int col, int rowMax, int colMax) {
    if(row == rowMax || col == colMax) return;
    char ch;
    cin >> ch;
    while (ch == '\n') cin >> ch;
    if (ch != 'D') {
        for (int i = row; i < rowMax; i++) {
            for (int j = col; j < colMax; j++) {
                bitmap[i][j] = ch;
            }
        }
        return;
    }
    int rowMid = (row + rowMax + 1)/2;
    int colMid =  (col + colMax + 1)/2;
    processD(row, col, rowMid, colMid);
    processD(row, colMid, rowMid, colMax);
    processD(rowMid, col, rowMax, colMid);
    processD(rowMid, colMid, rowMax, colMax);
}
int main() {
    char format;
    while (cin >> format) {

        if (format == '#') return 0;
        if (format == 'B') {
            int numRows, numCols;
            cin >> numRows >> numCols;
            for(int i = 0; i < numRows; i++) {
                for(int j = 0; j < numCols; j++) {
                    char ch;
                    cin >> ch;
                    if (ch == '\n') j--;
                    else {
                        bitmap[i][j] = ch;
                    }
                }
            }
            outPtr = output;
            processB(0, 0, numRows, numCols);
            *outPtr = 0;
            printf("D%4d%4d\n", numRows, numCols);

            int numPrinted = 0;
            char * wrtPtr = output;
            while(wrtPtr != outPtr) {
                cout << *wrtPtr++;
                numPrinted++;
                if(numPrinted == 50) {
                    cout << endl;
                    numPrinted = 0;
                }

            }
            if (numPrinted != 0) cout << endl;

        }
        else if (format == 'D') {
            int numRows, numCols;
            cin >> numRows >> numCols;
            processD(0, 0, numRows, numCols);
            printf("B%4d%4d\n", numRows, numCols);
            int numPrinted = 0;
            for(int i = 0; i < numRows; i++) {
                for(int j = 0; j < numCols; j++) {
                    cout << bitmap[i][j];
                    numPrinted++;
                    if(numPrinted == 50) {
                        cout << endl;
                        numPrinted = 0;
                    }

                }
            }
            if (numPrinted != 0) cout << endl;
        }
    }

}
