#include <cstdio>
#include <algorithm>
#include <iostream>
#include <utility>
#include <cstring>
#include <vector>
#include <bitset>
#include <cmath>

#define getD(x) scanf("%d", &x)
using namespace std;

int main() {
    int n; getD(n);
    int mat[100][100];
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            getD(mat[i][j]);
            if(i > 0) mat[i][j] += mat[i-1][j];
            if(j > 0) mat[i][j] += mat[i][j-1];
            if(i > 0 && j > 0) mat[i][j] -= mat[i-1][j-1];
        }
    }
    int bestSum = -99999999;
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            for(int k = i; k < n; k++) {
                for(int l = j; l < n; l++) {
                    int sum = mat[k][l];
                    if (i > 0) sum -= mat[i-1][l];
                    if (j > 0) sum -= mat[k][j-1];
                    if (i > 0 && j > 0) sum += mat[i-1][j-1];
                    bestSum = max(sum, bestSum);
                }
            }
        }
    }
    printf("%d\n", bestSum);

}
