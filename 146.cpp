#include <cstdio>
#include <algorithm>
#include <string>
#include <iostream>

using namespace std;

int main() {
    string line = "";
    getline(cin, line);
    while (line != "#") {

        string np;
        if(next_permutation(line.begin(), line.end())) {
            cout << line << endl;
        }
        else printf("No Successor\n");
        getline(cin, line);
    }


}
