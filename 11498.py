while True :
	k = int(input())
	if k == 0 : break

	n, m = map(int, input().split())
	for i in range(k) :
		x, y = map(int, input().split())
		x = x - n
		y = y - m
		if x == 0 or y == 0 :
			print('divisa')
		else :
			if x > 0 :
				if y > 0 :
					print('NE')
				else :
					print('SE')
			else :
				if y > 0 :
					print('NO')
				else :
					print('SO')
		