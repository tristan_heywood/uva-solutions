rfpNum = 1

while True :
    n, p = map(int, input().split())
    if n == 0 and p == 0 :
        break

    maxCompliance = 0
    propsWithMax = []
    for i in range(n) : input()
    for i in range(p) :
        newProp = input()
        cost, compliance = [(float(x[0]), int(x[1])) for x in [input().split()]][0]
        for j in range(compliance) : input()
        if compliance > maxCompliance :
            maxCompliance = compliance
            propsWithMax = [(newProp, cost)]
        elif compliance == maxCompliance :
            propsWithMax += [(newProp, cost)]

    if rfpNum != 1 :
        print()
    print('RFP #' + str(rfpNum))
    rfpNum += 1

    print(sorted(propsWithMax, key = lambda x : x[1])[0][0])
