while True :
	n, d, r = map(int, input().split())
	if n == d == r == 0 :
		break 
	mornLens = list(map(int, input().split()))
	eveLens = list(map(int, input().split()))
	mornLens.sort()
	eveLens.sort()
	totalExtra = sum(max(m + e - d, 0) for m, e in zip(mornLens, reversed(eveLens)))
	print(totalExtra * r)

