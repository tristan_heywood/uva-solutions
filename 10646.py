def val(sym) :
    try :
        return int(sym[0])
    except :
        return 10

for i in range(int(input())) :
    deck = input().split()
    hand = deck[-25:]
    pile = deck[:-25]
    y = 0

    for j in range(3) :
        x = val(pile[-1])
        y += x
        pile = pile[:-(10-x+1)]

    newDec = pile + hand
    print('Case {}: {}'.format(i + 1, newDec[y-1]))
