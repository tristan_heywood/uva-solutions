#include <cstdio>
#include <map>
#include <iostream>
#include <cstring>
#include <string>

using namespace std;

int main() {
	int t;
	scanf("%d", &t);
	string s;
	while(t--) {
		map<char, int> chMap;
		int k;
		scanf("%d", &k);
		int price;
		char ch;
		for(int i = 0; i < k; i++) {
			cin >> ch >> price;
			// scanf(" %c %d", &ch, &price);
			chMap[ch] = price;
		}
		int m;
		scanf("%d", &m);
		cin.ignore();
		long cost = 0;
		int len;
		while(m--) {
			string line;
			getline(cin, s);
			len = s.size();
			for(int i = 0; i < len; i++) {
				if(s[i] != ' ' and s[i] != '\n') {
					cost += chMap[s[i]];
				}
			}
		}

		// while((ch = getchar()) != '\n') {
		// 	if(ch != ' ' && ch != '\n') {
		// 		cost += chMap[ch];
		// 	}
		// }

		printf("%0.2f$\n", cost/100.0);
	}
}
