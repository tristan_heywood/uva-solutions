import itertools
import sys
first = True
for line in iter(sys.stdin.readline, '0\n') :


#while True :
	k, *s = map(int, line.split())

	games = []
	for com in itertools.combinations(s, 6) :
		games.append(com)  
	
	if not first :
		print()

	for g in sorted(games) : 
		print(' '.join((str(a) for a in g)))
	first = False