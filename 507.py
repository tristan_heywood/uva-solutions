gInt = lambda: int(input())
for tc in range(1, gInt() + 1):
    s = gInt()
    niceness = [gInt() for i in range(s-1)]

    maxPrev = 0
    prevStart = 1

    bestNice = -1e9
    bestSeg = None

    for i, n in enumerate(niceness, 1) :
        if maxPrev < 0 :
            prevStart = i
            maxPrev = n
        else :
            maxPrev = n + maxPrev
        if maxPrev > bestNice or (maxPrev == bestNice and (prevStart == bestSeg[0] or i - prevStart > bestSeg[1] - bestSeg[0])):
            bestNice = maxPrev
            bestSeg = (prevStart, i + 1)
    if bestNice > 0 :
        print('The nicest part of route {} is between stops {} and {}'.format(tc, *bestSeg))
    else :
        print('Route {} has no nice parts'.format(tc))
