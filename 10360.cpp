#include <cstdio>
#include <algorithm>
using namespace std;

#define getD(x) scanf("%d", &x)

int killed[1024][1024];


int main() {
    int row, col, tc;
    int d, n;

    getD(tc);
    while(tc--) {


        getD(d);
        getD(n);
        for(row = 0; row < 1024; row++) {
            for(col = 0; col < 1024; col++) {
                killed[row][col] = 0;
            }
        }

        for(int i = 0; i < n; i++) {
            int x, y, popSize;
            getD(x), getD(y), getD(popSize);
            for(row = max(0, x - d); row < min(1024, x + d + 1); row++) {
                for(col = max(0, y - d); col < min(1024, y + d + 1); col++) {
                    killed[row][col] += popSize;
                }
            }
        }
        int maxKilled = 0;
        int maxX = 0;
        int maxY = 0;

        for(row = 0; row < 1024; row++) {
            for(col = 0; col < 1024; col++) {
                if(killed[row][col] > maxKilled) {
                    maxKilled = killed[row][col];
                    maxX = row;
                    maxY = col;
                }
            }
        }
        printf("%d %d %d\n", maxX, maxY, maxKilled);

    }
}
