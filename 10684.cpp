#include <cstdio>
using namespace std; 

#define max(A, B) (A > B ? A : B)

int main() {
	int n; 
	int bets[100000]; 

	while(scanf("%d", &n), n) {
		for(int i = 0; i < n; i++) {
			scanf("%d", &bets[i]); 
		}

		int prevBest = max(0, bets[0]); 
		int totalBest = prevBest; 

		for(int i = 1; i < n; i++) {
			int curBest = max(prevBest + bets[i], bets[i]); 
			totalBest = max(curBest, totalBest); 
			prevBest = curBest;
		}
	
		if (totalBest > 0) {
			printf("The maximum winning streak is %d.\n", totalBest); 
		}
		else {
			printf("Losing streak.\n"); 
		}
	}
}