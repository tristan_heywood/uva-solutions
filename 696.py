import math
while True :
    m, n = map(int, input().split())
    if m == 0 and n == 0 : break
    num = 0
    if m * n == 0 :
        num = 0
    elif m == 1 or n == 1 :
        num = m * n
    elif m * n <= 6 :
        num = 4
    elif m == 2 or n == 2:
        larger = m * n // 2
        num = (larger // 4) * 4 + min(larger % 4, 2) * 2 
    else :
        num = math.ceil(m * n / 2)
    print('{} knights may be placed on a {} row {} column board.'.format(num, m, n))
