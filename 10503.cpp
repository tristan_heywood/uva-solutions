#include <cstdio>
#include <algorithm>
#include <iostream>
#include <utility>
#include <cstring>

#define getD(x) scanf("%d", &x)
using namespace std;

int n, m;

pair<int, int> dominos[14];
bool used[14];

bool isPossible(int left, int right, int numSpaces) {
    if(numSpaces == 0 && left == right) return true;

    for(int i = 0; i < m; i++) {
        if(!used[i]) {
            if(dominos[i].first == left) {
                used[i] = true;
                if(isPossible(dominos[i].second, right, numSpaces - 1)) return true;
            }
            else if(dominos[i].second == left) {
                used[i] = true;
                if(isPossible(dominos[i].first, right, numSpaces - 1)) return true;
            }
            used[i] = false;
        }
    }
    return false;
}

int main() {
    while(getD(n), n) {
        memset(used, 0, 14 * sizeof(used[0]));
        getD(m);
        int a, b; getD(a); getD(b);
        pair<int, int> left = make_pair(a, b);
        getD(a); getD(b);
        pair<int, int> right = make_pair(a, b);

        for(int i = 0; i < m; i++) {
            getD(a); getD(b);
            dominos[i] = make_pair(a, b);
        }
        if(isPossible(left.second, right.first, n)) {
            printf("YES\n");
        }
        else printf("NO\n");
    }
}
