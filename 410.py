import sys
import itertools
for setNum, ((c, s), specims) in enumerate(((list(map(int, x.split())) for x in pairLines) for pairLines in zip(*(iter(sys.stdin.readlines()),) * 2)), 1) :
    specims.sort(reverse = True)
    chambers = [0 for i in range(c)]
    chambers = list(itertools.zip_longest(chambers, reversed(specims[:c]), specims[c:]))
    print('Set #{}'.format(setNum))
    for chNum, chamber in enumerate(chambers) :
        print(' {}:'.format(chNum), *sorted([c for c in chamber if c not in [None, 0]]))
    AM = sum(specims) / c
    imbal = sum(abs(sum((c for c in ch if c is not None)) - AM) for ch in chambers)
    print('IMBALANCE = {:.5f}\n'.format(imbal))
