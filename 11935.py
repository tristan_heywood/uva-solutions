def isPossible(events, tankCapacity) :
    distTravelled = 0

    consumptionRate = 0
    numLeaks = 0
    fuelLeft = tankCapacity

    for event in events :
        evDist = int(event[0])
        distDelta = evDist - distTravelled
        fuelLeft -= consumptionRate * distDelta/100
        fuelLeft -= numLeaks * distDelta
        if fuelLeft < 0 :
            return False

        if event[1] == 'Fuel' :
            consumptionRate = int(event[-1])
        elif event[1] == 'Goal' :
            return True
        elif event[1] == 'Leak':
            numLeaks += 1
        elif event[1] == 'Mechanic' :
            numLeaks = 0
        elif event[1] == 'Gas' :
            fuelLeft = tankCapacity
        distTravelled = evDist


while True :
    events = []
    events.append(input().split())
    if events[-1][-1] == '0' : break

    line = input()
    while 'Goal' not in line :
        events.append(line.split())
        line = input()
    events.append(line.split())

    lo = 0
    hi = 1e6
    eps = 10**-5
    while(abs(hi - lo) > eps) :
        mid = (hi + lo) / 2
        if(isPossible(events, mid)) :
            hi = mid
        else :
            lo = mid
    print('{0:.3f}'.format(hi))
