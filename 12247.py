import itertools

def can_always_win(girl, boy) :
    for bOrd in itertools.permutations(boy) :
        for gOrd in itertools.permutations(girl) :
            if sum([1 if b > g else -1 for b, g in zip(bOrd, gOrd)]) < 1 :
                return False
    return True

while True :
    inp = input()
    if '0 0 0 0 0' in inp :
        break

    inp = list(map(int, inp.split()))
    gHand, bHand = inp[:3], inp[3:]

    for i in range(1, 53) :
        if i not in gHand and i not in bHand and can_always_win(gHand, bHand + [i]) :
            print(i)
            break
    else :
        print(-1)
