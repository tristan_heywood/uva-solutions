#include <cstdio>
#include <iostream>
#include <algorithm>
using namespace std;

int cars[2000];
int lis[2000]; //longest increasing subsequence starting at i
int lds[2000]; //longest decreasing subseqeuence starting at i

int main() {
    int tc; cin >> tc;
    while (tc--) {
        int n; cin >> n; 
        for (int i = 0; i < n; i++) cin >> cars[i];

        for (int i = n-1; i >= 0; i--) {
            int bestLen = 0;
            for (int j = i + 1; j < n; j++) {
                if (cars[j] >= cars[i]) bestLen = max(bestLen, lis[j]);
            }
            lis[i] = 1 + bestLen;
        }
        for (int i = n-1; i >= 0; i--) {
            int bestLen = 0;
            for (int j = i + 1; j < n; j++) {
                if (cars[j] <= cars[i]) bestLen = max(bestLen, lds[j]);
            }
            lds[i] = 1 + bestLen;
        }

        int bestLen = 0;
        for (int i = 0; i < n; i++) {
            bestLen = max(bestLen, lis[i] + lds[i] -1);
        }
        cout << bestLen << endl;

    }
}
