#include <cstdio>
#include <algorithm>

using namespace std;

#define getD(x) scanf("%d", &x)

int main() {
    int n;
    int vertWeights[1 << 15];
    int vertPotencies[1 << 15];
    while(getD(n) == 1) {
        for(int i = 0; i < 1 << n; i++) {
            getD(vertWeights[i]);
        }
        for (int i = 0; i < 1 << n; i++) {
            int potency = 0;
            for (int j = 0; j < n; j++) {
                potency += vertWeights[i ^ (1 << j)];
            }
            vertPotencies[i] = potency;
        }
        int maxPot = -1;
        for (int i = 0; i < 1 << n; i++) {
            int adjMax = -1;
            for (int j = 0; j < n; j++) {
                adjMax = max(adjMax, vertPotencies[i ^ (1 << j)]);
            }
            maxPot = max(maxPot, adjMax + vertPotencies[i]);

        }
        printf("%d\n", maxPot);
    }


}
