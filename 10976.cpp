#include <cstdio>
#include <iostream>
#include <vector>

using namespace std;

int main() {
    int k;
    while(cin >> k) {
        vector<pair<int, int> > sols;
        int lastX = 0;
        if(k == 1) {
            sols.push_back(make_pair(2, 2));
        }
        else {
            for(int y = k + 1; y <= 2 * k; y++) {
                if(y * k % (y - k) == 0) {
                    int x = (y * k)/(y - k);
                    sols.push_back(make_pair(x, y));
                }
            }
        }

        printf("%d\n", (int)sols.size());
        for(int i = 0; i < sols.size(); i++) {
            int x = sols[i].first;
            int y = sols[i].second;
            printf("1/%d = 1/%d + 1/%d\n", k, x, y);

        }
    }
}
