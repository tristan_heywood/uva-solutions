import itertools
import functools
import time
first = True
while True :
	n = int(input())
	if n == 0 : break 

	pairs = []
	nums = map(int, list('0123456789'))

	#loopStart = time.time()

	for b in range(1234, 98765 // n + 1) :
		a = b * n
		x = str(a)
		y = str(b)
		x = x if len(x) == 5 else '0' + x
		y = y if len(y) == 5 else '0' + y
		if len(set(x) | set(y)) == 10 :
			pairs.append((a, b))

	# totalTime = 0
	
	# for seq in itertools.permutations(nums) :
	# 	start = time.time()

	# 	if seq[0] != 0 :
	# 		num = 0 
	# 		for s in seq :
	# 			num = num * 10 + s
	# 		a = num // 100000
	# 		b = num - a * 100000
	# 		if b * n == a :
	# 			pairs.append((a, b))
		

		# a, b = seq[:5], seq[5:]
		# a, b = [functools.reduce(lambda rst, d: rst * 10 + d, x) for x in [a, b]]
		# c = 0
		# d = 0
		# for i, j in zip(a, b) :
		# 	c = c * 10 + i
		# 	d = d * 10 + i
		# if d * n == a :
		# 	pairs.append((c, d))
		# if b * n == a :
		# 	pairs.append((a, b))

		# totalTime += time.time() - start

	#print(time.time() - loopStart)
	if not first:
		print()
		

	if len(pairs) == 0 : 
		print('There are no solutions for {}.'.format(n))
	else :
		print('\n'.join(('{:0>5} / {:0>5} = {}'.format(*x, n) for x in pairs)))
	first = False