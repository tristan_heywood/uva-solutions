#include <cstdio>
#define getD(x) scanf("%d", &x)
using namespace std;

int main() {
    int tc; getD(tc);
    while(tc--) {
        int n; getD(n);
        int listA[1000];
        int sum = 0;
        for(int i = 0; i < n; i++) {
            int ai; getD(ai);
            for(int j = 0; j < i; j++) {
                if(listA[j] <= ai) sum++;
            }
            listA[i] = ai;
        }
        printf("%d\n", sum);
    }
}
