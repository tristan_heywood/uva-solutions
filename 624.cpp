#include <cstdio>
#include <algorithm>
#include <iostream>

#define getD(x) scanf("%d", &x)
using namespace std;

int main() {
    int n;
    while(cin >> n) {
        int numTracks; getD(numTracks);
        int trackLengths[20];
        for(int i = 0; i < numTracks; i++) {
            getD(trackLengths[i]);
        }
        int bestSet = 0;
        int bestSum = -1;
        for(int tracks = 0; tracks < (1 << numTracks); tracks++) {
            int lengthSum = 0;
            for(int i = 0; i < numTracks; i++) {
                if((1 << i) & tracks) {
                    lengthSum += trackLengths[i];
                }
            }
            if(lengthSum <= n && lengthSum > bestSum) {
                bestSum = lengthSum;
                bestSet = tracks;
            }
        }
        for(int i = 0; i < numTracks; i++) {
            if((1 << i) & bestSet) printf("%d ", trackLengths[i]);
        }
        printf("sum:%d\n", bestSum);
    }
}
