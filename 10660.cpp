#include <cstdio>
#include <cstdlib>
#include <array>

#define getD(x) scanf("%d", &x)
using namespace std;

int main() {
    int tc; getD(tc);
    while(tc--) {
        int n; getD(n);
        int population[5][5];
        for(int i = 0; i < 5; i++) {
            for(int j = 0; j < 5; j++) {
                population[i][j] = 0;
            }
        }
        for(int i = 0; i < n; i++) {
            int x, y, pop;
            getD(x); getD(y); getD(pop);
            population[x][y] = pop;
        }
        array<int, 5> offices;
        long int bestWeight = 2 << 20;
        array<int, 5> bestLocs;
        for(int a = 0; a < 25; a++) {
            for(int b = a + 1; b < 25; b++) {
                for(int c = b + 1; c < 25; c++) {
                    for(int d = c + 1; d < 25; d++) {
                        for(int e = d + 1; e < 25; e++) {
                            long int configWeight = 0;
                            for(int x = 0; x < 5; x++) {
                                for(int y = 0; y < 5; y++) {
                                    if(population[x][y] != 0) {
                                        offices = {a, b, c, d, e};
                                        long int closestDist = 2 << 20;
                                        int pop = population[x][y];
                                        for(int i = 0; i < 5; i++) {
                                            int row = offices[i] / 5;
                                            int col = offices[i] % 5;
                                            long int dist = pop * (abs(row - x) + abs(col - y));
                                            closestDist = min(closestDist, dist);
                                        }
                                        configWeight += closestDist;
                                    }
                                }
                            }
                            if(configWeight < bestWeight) {
                                bestWeight = configWeight;
                                bestLocs = {a, b, c, d, e};
                            }
                        }
                    }
                }
            }
        }
        for(int i = 0; i < 5; i++) {
            if(i) putchar(' ');
            printf("%d", bestLocs[i]);
        }
        printf("\n");

    }

}
