#include <cstdio>
#include <algorithm>
#include <iostream>
#include <utility>
#include <cstring>
#include <vector>
#include <bitset>
#include <cmath>

#define getD(x) scanf("%d", &x)
using namespace std;

int vesselCaps[1000];
int n, m;
bool isPossible(double maxContainer) {
    double curContain = 0;
    int numContains = 1;
    for(int i = 0; i < n; i++) {
        if(curContain + vesselCaps[i] < maxContainer) {
            curContain += vesselCaps[i];
        } else {
            curContain = vesselCaps[i];
            numContains += 1;
            if(numContains > m) {
                return false;
            }
            if(vesselCaps[i] > maxContainer) {
                return false;
            }
        }
    }
    return true;
}

int main() {

    while(cin >> n >> m) {


        for(int i = 0; i < n; i++) {
            getD(vesselCaps[i]);
        }

        double hi = 10e10;
        double lo = 1;
        double mid = 0;


        while(fabs(hi - lo) > 10e-2) {
            mid = (hi + lo) / 2;
            if(isPossible(mid)) {
                hi = mid;
            }
            else {
                lo = mid;
            }
        }
        printf("%d\n", (int)round(hi));
    }
}
