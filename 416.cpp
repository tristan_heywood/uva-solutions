#include <cstdio>
#include <algorithm>
#include <iostream>
#include <utility>
#include <cstring>
#include <vector>
#include <bitset>

#define getD(x) scanf("%d", &x)
using namespace std;

char numbers[10] =
{0b01111110,
0b00110000,
0b01101101,
0b01111001,
0b00110011,
0b01011011,
0b01011111,
0b01110000,
0b01111111,
0b01111011
};


int n;

char isOn[11];

char ledIsDead;

bool isMatch(int countMax, int lineNum, bool first) {
    if(lineNum == n) {
        return true;
    }
    if(countMax == -1) {
        return false;
    }
    int dispNum;
    if(first) dispNum = 0;
    else dispNum = countMax;
    for( ; dispNum <= countMax; dispNum++) {
        //= leds not on for both but are on for line => on for line but not for display => impossible
        if((numbers[dispNum] & isOn[lineNum]) ^ isOn[lineNum]) {
            continue;
        }
        //otherwise line does not have any leds on that num also doesn't have on

        //line can not have any leds on that are dead
        if(isOn[lineNum] & ledIsDead) {
            continue;
        }

        //leds on in dispNum but not in line
        //i.e. not on for both but are on for dispNum
        char deadInLine = (numbers[dispNum] & isOn[lineNum]) ^ numbers[dispNum];
        char prevDeadLEDs = ledIsDead;
        ledIsDead |= deadInLine;
        if(isMatch(dispNum-1, lineNum+1, false)) {
            // cout << "Matched number: " << bitset<8>(numbers[dispNum]) << " (" << dispNum << ") " << " to line: " << bitset<8>(isOn[lineNum]) << endl;
            return true;
        }
        ledIsDead = prevDeadLEDs; //restore
    }
    return false;
}

int main() {
    while(getD(n), n != 0) {
        ledIsDead = 0;
        for(int i = 0; i < n; i++) {
            isOn[i] = 0;
            char line[7];
            scanf("%s", line);
            for(int j = 0; j < 7; j++) {
                if(line[j] == 'Y') isOn[i] |= 1 << (6-j);

            }
        }
        if(isMatch(9, 0, true)) {
            printf("MATCH\n");
        }
        else printf("MISMATCH\n");

    }
}
