import itertools
for tc in range(1, int(input()) + 1) :
    n, d = [int(x) for x in input().split()]
    stones = input().split()
    while len(stones) < n :
        stones += input().split()
    stones = [('B', 0)] + [(x[0], int(x[1])) for x in (y.split('-') for y in stones)] + [('B', d)]
    maxJump = 0
    notSunken = [True for x in range(len(stones))]
    stoneInd = 0
    while stoneInd < len(stones) - 1:
        if stones[stoneInd][0] == 'S' :
            notSunken[stoneInd] = False
        if stones[stoneInd+1][0] == 'B':
            maxJump = max(maxJump, stones[stoneInd+1][1] - stones[stoneInd][1])
            stoneInd += 1
        else :
            maxJump = max(maxJump, stones[stoneInd+2][1] - stones[stoneInd][1])
            stoneInd += 2
    stones = list(itertools.compress(stones, notSunken))
    mj = max((x[1] - y[1] for x, y in zip(stones[1:], stones)))
    print('Case {}: {}'.format(tc, max(maxJump, mj)))
