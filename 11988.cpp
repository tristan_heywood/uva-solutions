#include <iostream>
#include <list>
#include <iterator>
using namespace std;

int main() {

    string line;
    while(getline(cin, line)) {
        list<char> output;
        list<char>::iterator insert_it = output.begin();
        for (int i = 0; i < line.size(); i++) {
            if (line[i] == '[') {
                insert_it = output.begin();
            }
            else if (line[i] == ']') {
                insert_it = output.end();
            }
            else {
                output.insert(insert_it, line[i]);
            }
        }
        for (list<char>::iterator it = output.begin(); it != output.end(); it++) {
            cout << *it;
        }
        cout << endl;
    }
}
