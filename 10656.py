for n in iter(lambda: int(input()), 0) :
    output = [x for x in (int(input()) for i in range(n)) if x != 0]
    if len(output) > 0 : print(*output)
    else : print(0)
