import math
for _ in range(int(input())) :
    inp = input().split()
    piece, m, n = inp[0], int(inp[1]), int(inp[2])
    if piece == 'r' :
        print(min(m, n))
    elif piece == 'K' :
        print(math.ceil(m/2) * math.ceil(n/2))
    elif piece == 'Q' :
        print(min(m, n))
    else: print(math.ceil(m * n / 2))
