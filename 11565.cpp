#include <cstdio>

#define getD(x) scanf("%d", &x)
using namespace std;

int main() {
    int tc; getD(tc);
    while(tc--) {
        int a, b, c; getD(a); getD(b); getD(c);
        bool found = false;
        int x, y, z;
        for(x = -100; x <= 100; x++) {
            for(y = x + 1; y <= 100; y++) {
                for(z = y + 1; z <= 100; z++) {
                    if(x + y + z == a && x * y * z == b && x*x + y*y + z*z == c) {
                        found = true;
                        goto endloop;
                    }
                }
            }
        }
    endloop:
        if(found) {
            printf("%d %d %d\n", x, y, z);
        }
        else {
            printf("No solution.\n");
        }
    }
}
