#include <cstdio>
#include <algorithm>
#include <iostream>

#define getD(x) scanf("%d", &x)
using namespace std;

int ring[16];

int primes[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31};
int n;
bool isPrime(int x) {
    for(int i = 0; i < 11; i++) {
        if(primes[i] == x) return true;
    }
    return false;
}

void find_soln(int ringLen) {
    if(ringLen == n && isPrime(ring[ringLen-1] + ring[0])) {
        for(int i = 0; i < n-1; i++) {
            printf("%d ", ring[i]);
        }
        printf("%d\n", ring[ringLen-1]);
    }
    for(int i = 2; i <= n; i++) {
        if(!isPrime(i + ring[ringLen-1])) goto endloop;
        for(int j = 0; j < ringLen; j++) {
            if(ring[j] == i) goto endloop;
        }
        ring[ringLen] = i;
        find_soln(ringLen + 1);

        endloop:;
    }

}

int main() {
    int tc = 0;
    while(getD(n) == 1) {
        if(tc) printf("\n");
        printf("Case %d:\n", ++tc);
        ring[0] = 1;
        find_soln(1);

    }
}
