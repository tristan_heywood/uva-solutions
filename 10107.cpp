#include <cstdio>
#include <vector>
#include <algorithm>

#define getD(x) scanf("%d", &x)

using namespace std;

int main() {
    vector<int> nums;
    int i;
    while(getD(i) == 1) {
        nums.insert(upper_bound(nums.begin(), nums.end(), i), i);
        if (nums.size() % 2 == 0) {
            printf("%d\n", (nums[nums.size()/2 - 1] + nums[nums.size()/2])/2);
        }
        else printf("%d\n", nums[nums.size()/2]);
    }
}
