#include <iostream>
#include <bitset>
#include <cmath>

using namespace std;
int main() {
    int n;
    bitset<3000> seen;
    while (cin >> n) {
        if (n == 1) {
            int x; cin >> x;
            cout << "Jolly" << endl;
            continue;
        }
        seen.reset();
        int prev; cin >> prev;
        for(int i = 1; i < n; i++) {
            int x; cin >> x;
            seen[abs(x - prev)] = true;
            prev = x;
        }
        for(int i = 1; i < n; i++) {
            if(!seen[i]) {
                cout << "Not jolly" << endl;
                goto afterPrint;
            }
        }
        cout << "Jolly" << endl;
        afterPrint:;
    }
}
