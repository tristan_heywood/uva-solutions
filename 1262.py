def permute(columns) :
    if len(columns) == 1 :
        for letter in columns[0] :
            yield [letter]
    else :
        yield from ([letter] + perm for letter in columns[0] for perm in permute(columns[1:]) )

for tc in range(int(input())) :
    k = int(input())
    gridOne = [input() for i in range(6)]
    gridTwo = [input() for i in range(6)]

    colSets = [set(x[i] for x in gridOne).intersection(set(x[i] for x in gridTwo)) for i in range(5)]
    colLists = [list(sorted(x)) for x in colSets]

    for ind, perm in enumerate(permute(colLists), 1) :
        if ind == k :
            print(''.join(perm))
            break
    else :
        print('NO')
