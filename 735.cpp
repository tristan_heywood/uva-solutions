#include <cstdio>
#include <set>

#define getD(x) scanf("%d", &x)
using namespace std;

int scores[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 21, 27, 33, 39, 42, 45, 48, 51, 54, 57, 60, 50};
int numUnique[] = {0, 1, 3, 6};

int main() {
    int target;
    while(getD(target), target > 0) {
        int combinCount = 0;
        int permCount = 0;
        for(int i = 0; i < 43; i++) {
            for(int j = i; j < 43; j++) {
                for(int k = j; k < 43; k++) {
                    if (scores[i] + scores[j] + scores[k] == target) {
                        combinCount++;
                        set<int> scs{scores[i], scores[j], scores[k]};
                        permCount += numUnique[scs.size()];
                    }
                }
            }
        }
        if (combinCount == 0) {
            printf("THE SCORE OF %d CANNOT BE MADE WITH THREE DARTS.\n", target);
        }
        else {
            printf("NUMBER OF COMBINATIONS THAT SCORES %d IS %d.\n", target, combinCount);
            printf("NUMBER OF PERMUTATIONS THAT SCORES %d IS %d.\n", target, permCount);
        }
        printf("**********************************************************************\n");
    }
    printf("END OF OUTPUT\n");
}
