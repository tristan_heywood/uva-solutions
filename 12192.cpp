#include <cstdio>
#include <algorithm>

using namespace std;

int main() {
	int n, m, q, l, u; 
	for(;;) {
		scanf("%d %d", &n, &m); 
		if(n == 0 && m == 0) break; 
		int plot[n][m]; 
		for(int i = 0; i < n; i++) {
			for(int j = 0; j < m; j++) {
				scanf("%d", &plot[i][j]);
			}
		}
		scanf("%d", &q); 
		for(int i = 0; i < q; i++) {
			scanf("%d %d", &l, &u); 
			int maxLen = 0; 
			for(int j = 0; j < n; j++) {
				int * topLeftPtr = lower_bound(plot[j], plot[j] + m, l);
				int rowInd = j + maxLen;
				int colInd = topLeftPtr - plot[j] + maxLen;
				while(rowInd < n && colInd < m && plot[rowInd][colInd] <= u) {
					rowInd++; 
					colInd++; 
				}
				maxLen = rowInd - j; 
			}
			printf("%d\n", maxLen); 
		}
		printf("-\n"); 
	}
	

}