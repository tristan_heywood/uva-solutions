#include <cstdio>
#include <algorithm>
#include <iostream>
#include <utility>
#include <cstring>
#include <vector>
#include <bitset>
#include <cmath>

#define getD(x) scanf("%d", &x)
using namespace std;

int main() {
    long long n, l, u, mask;
    while(cin >> n >> l >> u) {
        mask = 0;
        for(long long i = 31; i >= 0; i--) {
            if (n & (1LL << i)) {
                if(mask + (1LL << i) <= l) mask += (1LL << i);
            }
            else {
                if (mask + (1LL << i) <= u) mask += (1LL << i);
            }
        }
        printf("%lld\n", mask);
    }
}
