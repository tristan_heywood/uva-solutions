#include <cstdio>
#include <algorithm>

#define getD(x) scanf("%d", &x)
using namespace std;

int main() {
    int tc; getD(tc);
    while(tc--) {
        int n; getD(n);
        int M[8][8];
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                getD(M[i][j]);
            }
        }
        int choice[8] = {0, 1, 2, 3, 4, 5, 6, 7};
        int bestTaken =  10000000;
        do {
            int numTaken = 0;
            for(int i = 0; i < n; i++) {
                numTaken += M[i][choice[i]];

            }
            bestTaken = min(bestTaken, numTaken);

        } while(next_permutation(choice, choice + n));
        printf("%d\n", bestTaken);
    }

}
