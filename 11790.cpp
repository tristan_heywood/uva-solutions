#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int tc; cin >> tc;
    int caseNum = 0;
    while(tc--) {
        caseNum++;
        int n; cin >> n;
        vector<int> heights;
        vector<int> widths;
        vector<int> lis; //longest increasing subseqeuence ending at i
        vector<int> lds; //longest decreasing subsequence ending at i
        for (int i = 0; i < n; i++) {
            int h; cin >> h;
            heights.push_back(h);
        }
        for (int i = 0; i < n; i++) {
            int w; cin >> w;
            widths.push_back(w);
        }
        int longestIncr = 0;
        int longestDecr = 0;
        for (int i = 0; i < n; i++) {
            int lisBest = 0;
            int ldsBest = 0;
            for (int j = 0; j < i; j++) {
                if (heights[j] < heights[i]) lisBest = max(lisBest, lis[j]);
                if (heights[j] > heights[i]) ldsBest = max(ldsBest, lds[j]);
            }
            lis.push_back(lisBest + widths[i]);
            lds.push_back(ldsBest + widths[i]);
            longestIncr = max(longestIncr, lis[i]);
            longestDecr = max(longestDecr, lds[i]);
        }
        if (longestIncr >= longestDecr) {
            printf("Case %d. Increasing (%d). Decreasing (%d).\n", caseNum, longestIncr, longestDecr);
        }
        else {
            printf("Case %d. Decreasing (%d). Increasing (%d).\n", caseNum, longestDecr, longestIncr);
        }

    }
}
